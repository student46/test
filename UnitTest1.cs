using System;
using System.Collections.Generic;
using CourseAutomatization.Entity;
using CourseAutomatization.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CourseAutomatization.Tests
{
    [TestClass]
    public class frmProfEditTests: TestsConfig
    {
        //correct
        [TestMethod]
        public void AddProfessorVer1()
        {
            InitSystem();

            ProfessorRepository professorRepo = new ProfessorRepository();

            var before = professorRepo.GetList().Count;

            frmProfEdit frm = new frmProfEdit(professorRepo);
            frm.txtSurname.Text = "Фамилия";
            frm.txtName.Text = "Имя";
            frm.txtPatronimyc.Text = "Отчество";
            frm.txtAge.Text = "43";
            frm.txtExperience.Text = "14";
            frm.txtPaspSeries.Text = "2219";
            frm.txtPaspNumber.Text = "1212";
            frm.txtAddress.Text = "Москва, ул.Ленина";
            frm.txtPhone.Text = "89999999999";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = professorRepo.GetList().Count;

            Assert.IsTrue(after > before);
        }

        //incorrect
        [TestMethod]
        public void AddProfessorVer2()
        {
            InitSystem();

            ProfessorRepository professorRepo = new ProfessorRepository();

            var before = professorRepo.GetList().Count;

            frmProfEdit frm = new frmProfEdit(professorRepo);
            frm.txtSurname.Text = "Фамилия";
            frm.txtName.Text = "Имя";
            frm.txtPatronimyc.Text = "Отчество";
            frm.txtAge.Text = "143";
            frm.txtExperience.Text = "150abc";
            frm.txtPaspSeries.Text = "2219hello";
            frm.txtPaspNumber.Text = "1212world";
            frm.txtAddress.Text = "Москва, ул.Ленина";
            frm.txtPhone.Text = "89999999999---";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = professorRepo.GetList().Count;

            Assert.IsTrue(after == before);
        }

    }

    [TestClass]
    public class frmPaymentEditTests: TestsConfig
    {
        //correct
        [TestMethod]
        public void AddPaymentVer1()
        {
            InitSystem();

            PaymentRepository paymentRepository = new PaymentRepository();

            var before = paymentRepository.GetList().Count;

            frmPaymentEdit frm = new frmPaymentEdit(paymentRepository);
            frm.txtPayment.Text = "1000";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = paymentRepository.GetList().Count;

            Assert.IsTrue(after > before);
        }

        //incorrect
        [TestMethod]
        public void AddPaymentVer2()
        {
            InitSystem();

            PaymentRepository paymentRepository = new PaymentRepository();

            var before = paymentRepository.GetList().Count;

            frmPaymentEdit frm = new frmPaymentEdit(paymentRepository);
            frm.txtPayment.Text = "Hello!---";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = paymentRepository.GetList().Count;

            Assert.IsTrue(after == before);
        }

    }

    [TestClass]
    public class frmGroupEditTests: TestsConfig
    {
        //correct
        [TestMethod]
        public void AddGroupVer1()
        {
            InitSystem();

            GroupRepository groupRepository = new GroupRepository();

            var before = groupRepository.GetList().Count;

            frmGroupEdit frm = new frmGroupEdit(groupRepository);
            frm.txtSpeciality.Text = "Spec";
            frm.txtStudentCount.Text = "10";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = groupRepository.GetList().Count;

            Assert.IsTrue(after > before);
        }

        //incorrect
        [TestMethod]
        public void AddGroupVer2()
        {
            InitSystem();

            GroupRepository groupRepository = new GroupRepository();

            var before = groupRepository.GetList().Count;

            frmGroupEdit frm = new frmGroupEdit(groupRepository);
            frm.txtSpeciality.Text = "Spec1-1!!!2";
            frm.txtStudentCount.Text = "10hello";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = groupRepository.GetList().Count;

            Assert.IsTrue(after == before);
        }

    }

    [TestClass]
    public class frmLoadingEditTests: TestsConfig
    {
        //correct
        [TestMethod]
        public void AddLoadingVer1()
        {
            InitSystem();

            //добавляем преподавателя и группу
            new ProfessorRepository().Add(new Professor() { FirstName="FirstName",
                LastName="LastName", 
                Address="Aderess",
                Age=45, 
                Experience=10,
                PaspNumber="1234",
                PaspSeries="1234",
                Patronimyc="Hello", 
                Phone="phone" });

            new GroupRepository().Add(new Group() { Speciality = "Test", StudentCount = 10 });

            //добавляем нагрузку
            LoadingRepository loadingRepository = new LoadingRepository();

            var before = loadingRepository.GetList().Count;

            frmLoadingEdit frm = new frmLoadingEdit(loadingRepository);
            frm.txtHours.Text = "10";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = loadingRepository.GetList().Count;

            Assert.IsTrue(after > before);
        }

        //incorrect
        [TestMethod]
        public void AddLoadingVer2()
        {
            InitSystem();

            //добавляем преподавателя и группу
            new ProfessorRepository().Add(new Professor()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Address = "Aderess",
                Age = 45,
                Experience = 10,
                PaspNumber = "1234",
                PaspSeries = "1234",
                Patronimyc = "Hello",
                Phone = "phone"
            });
            new GroupRepository().Add(new Group() { Speciality = "Test", StudentCount = 10 });

            //добавляем нагрузку
            LoadingRepository loadingRepository = new LoadingRepository();

            var before = loadingRepository.GetList().Count;

            frmLoadingEdit frm = new frmLoadingEdit(loadingRepository);
            frm.txtHours.Text = "hello";

            frm.btnSave_Click(new object(), new EventArgs());

            var after = loadingRepository.GetList().Count;

            Assert.IsTrue(after == before);
        }



    }
    [TestClass]
    public class frmFinReportEditTests : TestsConfig
    {
        //correct
        [TestMethod]
        public void AddFinReportVer1()
        {
            InitSystem();

            //добавляем преподавателя, группу и оплату
            new ProfessorRepository().Add(new Professor()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Address = "Aderess",
                Age = 45,
                Experience = 10,
                PaspNumber = "1234",
                PaspSeries = "1234",
                Patronimyc = "Hello",
                Phone = "phone"
            });
            new GroupRepository().Add(new Group() { Speciality = "Test", StudentCount = 10 });
            new PaymentRepository().Add(new Payment() { PayPerHour = 1000 });

            //добавляем нагрузку
            LoadingRepository loadingRepository = new LoadingRepository();
            frmLoadingEdit frmLoadingEdit = new frmLoadingEdit(loadingRepository);
            frmLoadingEdit.txtHours.Text = "10";
            frmLoadingEdit.btnSave_Click(new object(), new EventArgs());

            //добавляем отчет
            FinReportRepository finReportRepository = new FinReportRepository();
            var before = finReportRepository.GetList().Count;

            frmFinReportEdit frmFinReport = new frmFinReportEdit(finReportRepository);
            frmFinReport.btnAdd_Click(new object(), new EventArgs());
            frmFinReport.btnSave_Click(new object(), new EventArgs());

            var after = finReportRepository.GetList().Count;

            Assert.IsTrue(after > before);
        }

        //incorrect
        [TestMethod]
        public void AddFinReportVer2()
        {
            InitSystem();

            //добавляем отчет, не добавляя нужные для отчёта параметры
            FinReportRepository finReportRepository = new FinReportRepository();
            var before = finReportRepository.GetList().Count;

            frmFinReportEdit frmFinReport = new frmFinReportEdit(finReportRepository);
            frmFinReport.btnAdd_Click(new object(), new EventArgs());
            frmFinReport.btnSave_Click(new object(), new EventArgs());

            var after = finReportRepository.GetList().Count;

            Assert.IsTrue(after == before);
        }



    }

    public class TestsConfig : ITestsConfig
    {
        public void InitSystem()
        {
            InitRepositories();
        }
        private void InitRepositories()
        {
            new FinReportRepository().DeleteAll();
            new GroupRepository().DeleteAll();
            new LoadingRepository().DeleteAll();
            new PaymentRepository().DeleteAll();
            new ProfessorRepository().DeleteAll();
        }

    }

    interface ITestsConfig
    {
        void InitSystem();
    }

}
